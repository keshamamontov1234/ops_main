import logging
from pathlib import Path

import click
import pandas as pd


def _remove_whitespace_from_columns(df: pd.DataFrame) -> pd.DataFrame:
    df.columns = df.columns.str.strip()
    return df


def _drop_duplicates(df: pd.DataFrame) -> pd.DataFrame:
    return df.drop_duplicates()


def _strip_text_columns(df: pd.DataFrame) -> pd.DataFrame:
    text_columns = df.select_dtypes(include=['object']).columns
    for col in text_columns:
        df[col] = df[col].str.strip()
    return df


def _fill_missing_values(df: pd.DataFrame) -> pd.DataFrame:
    text_columns = df.select_dtypes(include=['object']).columns
    for col in text_columns:
        df[col] = df[col].fillna("Unknown")
    return df


@click.command()
@click.option("--input-filepath", type=click.Path(exists=True, path_type=Path))
@click.option("--output-filepath", type=click.Path(path_type=Path), default="dataset/processed/cleaned_data.csv")
def main(input_filepath: Path, output_filepath: Path):
    logger = logging.getLogger(__name__)
    logger.info("Starting data cleaning process")

    df = pd.read_csv(input_filepath)

    # Remove whitespace from columns
    df = _remove_whitespace_from_columns(df)

    # Drop duplicates
    df = _drop_duplicates(df)

    # Strip text columns
    df = _strip_text_columns(df)

    # Fill missing values
    df = _fill_missing_values(df)

    df.to_csv(output_filepath, index=False)
    logger.info("Finished data cleaning process")


if __name__ == "__main__":
    main()
