import logging
from pathlib import Path
import os
import json
import click
from dotenv import find_dotenv, load_dotenv
import zipfile


@click.command()
@click.option("--dataset-url", type=str, required=True)
@click.option("--output-filepath", type=click.Path(path_type=Path), required=True)
def main(dataset_url: str, output_filepath: Path) -> None:
    """Download raw dataset from Kaggle."""
    logger = logging.getLogger(__name__)
    logger.info("Starting download from Kaggle")

    # Load environment variables
    load_dotenv(find_dotenv())

    # Set Kaggle API credentials manually
    kaggle_username = os.getenv('KAGGLE_USERNAME')
    kaggle_key = os.getenv('KAGGLE_KEY')

    if kaggle_username is None or kaggle_key is None:
        raise ValueError("KAGGLE_USERNAME and KAGGLE_KEY must be set")

    # Create kaggle.json file
    kaggle_config_dir = os.path.expanduser("~/.kaggle")
    os.makedirs(kaggle_config_dir, exist_ok=True)
    kaggle_json_path = os.path.join(kaggle_config_dir, "kaggle.json")

    with open(kaggle_json_path, "w") as f:
        json.dump({"username": kaggle_username, "key": kaggle_key}, f)

    # Set permissions to read and write for user only
    os.chmod(kaggle_json_path, 0o600)

    from kaggle.api.kaggle_api_extended import KaggleApi

    # Extract the dataset name from the URL
    dataset_name = dataset_url.split("/")[-1]

    # Initialize Kaggle API with the kaggle.json file
    api = KaggleApi()
    api.authenticate()

    # Download the dataset
    api.dataset_download_files(dataset=dataset_name, path=output_filepath, quiet=False, unzip=False)
    logger.info("Finished downloading dataset")

    # Unzip the downloaded file
    zip_path = output_filepath / f"{dataset_name}.zip"
    with zipfile.ZipFile(zip_path, 'r') as zip_ref:
        zip_ref.extractall(output_filepath)

    # Check the contents of the directory
    extracted_files = list(output_filepath.glob('*'))
    logger.info(f"Extracted files: {extracted_files}")

    # Clean up the kaggle.json file
    os.remove(kaggle_json_path)

    # Verify that the expected file exists
    expected_file = output_filepath / "pricerunner_aggregate.csv"
    if not expected_file.exists():
        raise FileNotFoundError(f"Expected file {expected_file} does not exist. Extracted files: {extracted_files}")


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levellevel)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    main()
