import pandas as pd
from sklearn.model_selection import train_test_split
import click
from pathlib import Path


@click.command()
@click.option("--input-filepath", type=click.Path(exists=True, path_type=Path))
@click.option("--output-dir", type=click.Path(path_type=Path))
def main(input_filepath: Path, output_dir: Path):
    df = pd.read_csv(input_filepath)
    X = pd.DataFrame(df["Cluster Label"])
    y = df["Category ID"]

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=42
    )

    X_train.to_csv(output_dir / "X_train.csv", index=False)
    X_test.to_csv(output_dir / "X_test.csv", index=False)
    y_train.to_csv(output_dir / "y_train.csv", index=False)
    y_test.to_csv(output_dir / "y_test.csv", index=False)


if __name__ == "__main__":
    main()
