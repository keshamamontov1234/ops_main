import pandas as pd
from sklearn.model_selection import GridSearchCV
from catboost import CatBoostClassifier
import click
from pathlib import Path
import joblib


@click.command()
@click.option("--train-dir", type=click.Path(exists=True, path_type=Path), help="Directory containing the train sets")
@click.option("--output-dir", type=click.Path(path_type=Path), help="Directory to save the best hyperparameters")
def main(train_dir: Path, output_dir: Path):
    # Ensure the output directory exists
    output_dir.mkdir(parents=True, exist_ok=True)

    # Создание отдельной директории для catboost_info/select_hyperparams
    catboost_info_dir = output_dir / "catboost_info/select_hyperparams"
    catboost_info_dir.mkdir(parents=True, exist_ok=True)

    X_train = pd.read_csv(train_dir / "X_train.csv")
    y_train = pd.read_csv(train_dir / "y_train.csv")

    cat = CatBoostClassifier(
        random_seed=42,
        logging_level="Silent",
        task_type="CPU",
        text_features=["Cluster Label"],
        train_dir=catboost_info_dir,
    )

    # Ограничим количество гиперпараметров для поиска
    param_grid = {
        'iterations': [10, 20],  # Меньшее количество итераций для снижения нагрузки
        'learning_rate': [0.1, 0.2]  # Два значения скорости обучения
    }

    grid_search = GridSearchCV(estimator=cat, param_grid=param_grid, cv=3, scoring='accuracy', verbose=1, n_jobs=-1)
    grid_search.fit(X_train, y_train)

    best_params = grid_search.best_params_
    best_score = grid_search.best_score_

    print(f"Best parameters found: {best_params}")
    print(f"Best cross-validation score: {best_score}")

    # Save the best parameters
    with open(output_dir / "best_params.joblib", "wb") as f:
        joblib.dump(best_params, f)


if __name__ == "__main__":
    main()
