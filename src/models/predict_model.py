import pandas as pd
from sklearn.metrics import classification_report
import click
from pathlib import Path
import joblib


@click.command()
@click.option("--test-dir", type=click.Path(exists=True, path_type=Path), default="dataset/split")
@click.option("--model-path", type=click.Path(exists=True, path_type=Path), default="models/catboost_model.joblib")
@click.option("--output-filepath", type=click.Path(path_type=Path), default="dataset/results/predictions.csv")
def main(test_dir: Path, model_path: Path, output_filepath: Path):
    X_test = pd.read_csv(test_dir / "X_test.csv")
    y_test = pd.read_csv(test_dir / "y_test.csv")

    cat = joblib.load(model_path)
    pred = cat.predict(X_test)

    # Убедимся, что y_test и pred являются одномерными
    y_test = y_test.squeeze()
    pred = pred.squeeze()

    predictions_df = pd.DataFrame({"y_test": y_test, "pred": pred})
    output_filepath.parent.mkdir(parents=True, exist_ok=True)
    predictions_df.to_csv(output_filepath, index=False)
    print(classification_report(y_test, pred))


if __name__ == "__main__":
    main()
