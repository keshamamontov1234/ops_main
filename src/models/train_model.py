import pandas as pd
from catboost import CatBoostClassifier
import joblib
import click
from pathlib import Path
import mlflow
import mlflow.pyfunc
import os
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score
from dotenv import load_dotenv


load_dotenv()


class ModelWrapper(mlflow.pyfunc.PythonModel):
    def load_context(self, context):
        self.model = joblib.load(context.artifacts["model"])

    def predict(self, context, model_input):
        return self.model.predict(model_input)


@click.command()
@click.option("--train-dir", type=click.Path(exists=True, path_type=Path), help="Directory containing the train sets")
@click.option("--output-dir", type=click.Path(path_type=Path), help="Directory to save the trained model")
@click.option("--params-path", type=click.Path(exists=True, path_type=Path), help="Path to the best hyperparameters")
@click.option("--test-dir", type=click.Path(exists=True, path_type=Path), help="Directory containing the test sets")
def main(train_dir: Path, output_dir: Path, params_path: Path, test_dir: Path):
    # Ensure the output directory exists
    output_dir.mkdir(parents=True, exist_ok=True)

    # Создание отдельной директории для catboost_info/train_model
    catboost_info_dir = output_dir / "catboost_info/train_model"
    catboost_info_dir.mkdir(parents=True, exist_ok=True)

    X_train = pd.read_csv(train_dir / "X_train.csv")
    y_train = pd.read_csv(train_dir / "y_train.csv")
    X_test = pd.read_csv(test_dir / "X_test.csv")
    y_test = pd.read_csv(test_dir / "y_test.csv")

    best_params = joblib.load(params_path)

    cat = CatBoostClassifier(
        **best_params,
        random_seed=42,
        logging_level="Silent",
        task_type="CPU",
        text_features=["Cluster Label"],
        train_dir=catboost_info_dir,
    )

    # Set the tracking URI to point to the remote MLflow server
    mlflow.set_tracking_uri(os.getenv("MLFLOW_TRACKING_URI"))

    with mlflow.start_run():
        cat.fit(X_train, y_train)

        # Log parameters
        mlflow.log_params(best_params)

        # Save the model
        model_path = output_dir / "catboost_model.joblib"
        joblib.dump(cat, model_path)

        # Predict and evaluate on test data
        y_pred = cat.predict(X_test)
        test_accuracy = accuracy_score(y_test, y_pred)
        test_f1 = f1_score(y_test, y_pred, average='weighted')
        test_precision = precision_score(y_test, y_pred, average='weighted')
        test_recall = recall_score(y_test, y_pred, average='weighted')

        # Log test metrics
        mlflow.log_metric("test_accuracy", test_accuracy)
        mlflow.log_metric("test_f1_score", test_f1)
        mlflow.log_metric("test_precision", test_precision)
        mlflow.log_metric("test_recall", test_recall)

        # Log the model
        mlflow.pyfunc.log_model(
            artifact_path="catboost_model",
            python_model=ModelWrapper(),
            artifacts={"model": str(model_path)},
            registered_model_name="catboost_model",
        )

        # Print metrics to console
        print(f"Test Accuracy: {test_accuracy}")
        print(f"Test F1 Score: {test_f1}")
        print(f"Test Precision: {test_precision}")
        print(f"Test Recall: {test_recall}")


if __name__ == "__main__":
    main()
