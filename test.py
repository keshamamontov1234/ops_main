import boto3
import os
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()

# Fetch AWS credentials from environment variables
aws_access_key = os.getenv('AWS_ACCESS_KEY_ID')
aws_secret_key = os.getenv('AWS_SECRET_ACCESS_KEY')
minio_endpoint = 'http://94.241.142.25:9000/'

# Print AWS credentials and endpoint for verification
print(f"AWS_ACCESS_KEY_ID: {aws_access_key}")
print(f"AWS_SECRET_ACCESS_KEY: {aws_secret_key}")
print(f"MinIO Endpoint: {minio_endpoint}")

# Initialize boto3 session
session = boto3.Session(
    aws_access_key_id=aws_access_key,
    aws_secret_access_key=aws_secret_key,
)

# Create S3 client
s3_client = session.client(
    service_name='s3',
    endpoint_url=minio_endpoint
)

# Test S3 client by listing buckets
try:
    response = s3_client.list_buckets()
    print("Buckets:", response['Buckets'])
except Exception as e:
    print("Error accessing MinIO:", str(e))
